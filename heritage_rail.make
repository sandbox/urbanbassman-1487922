includes[openoutreach] = "http://drupalcode.org/project/openoutreach.git/blob_plain/refs/tags/7.x-1.0-beta8:/openoutreach.make"

includes[debut] = "debut_overrides.make.inc"
includes[debut_event] = "debut_event_overrides.make.inc"

projects[stock_list][type] = "module"
projects[stock_list][download][type] = "git"
projects[stock_list][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1485926.git"
projects[stock_list][subdir] = "custom"

projects[locomotive][type] = "module"
projects[locomotive][download][type] = "git"
projects[locomotive][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1489942.git"
projects[locomotive][subdir] = "custom"

projects[locomotive_sample_content][type] = "module"
projects[locomotive_sample_content][download][type] = "git"
projects[locomotive_sample_content][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1493122.git"
projects[locomotive_sample_content][subdir] = "custom"

projects[article_sample_content][type] = "module"
projects[article_sample_content][download][type] = "git"
projects[article_sample_content][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1503180.git"
projects[article_sample_content][subdir] = "custom"

projects[heritage_rail_home_page][type] = "module"
projects[heritage_rail_home_page][download][type] = "git"
projects[heritage_rail_home_page][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1503228.git"
projects[heritage_rail_home_page][subdir] = "custom"

projects[backup_migrate][type] = "module"
projects[backup_migrate][subdir] = "custom"
projects[backup_migrate][version] = "2.2"

projects[features_extra][type] = "module"
projects[features_extra][subdir] = "custom"
projects[features_extra][version] = "1.x-dev"

projects[node_export][type] = "module"
projects[node_export][subdir] = "custom"
projects[node_export][version] = "3.x-dev"
projects[uuid][type] = "module"
;projects[uuid][subdir] = "custom"
;projects[uuid][version] = "1.0-alpha3"
projects[uuid][download][type] = "get"
projects[uuid][download][url] = "http://ftp.drupal.org/files/projects/uuid-7.x-1.x-dev.tar.gz"

;theme
projects[omega][type] = "module"
projects[omega][subdir] = "custom"

projects[navin][type] = "module"
projects[navin][subdir] = "custom"

projects[lner][type] = "theme"
projects[lner][download][type] = "git"
projects[lner][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1493136.git"
projects[lner][subdir] = "custom"

;omega supporting modules
projects[omega_tools][type] = "module"
projects[omega_tools][subdir] = "custom"
projects[delta][type] = "module"
projects[delta][subdir] = "custom"

